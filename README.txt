Droplist Filter (Drupal module)
http://joshuachan.ca/home/droplist-filter/

Copyright 2008 Joshua Chan
This Javascript class is distributed under the terms of the GNU General Public
License.


USAGE
-----

Install and configure this like a normal Drupal module. Place the main 
droplist_filter directory into drupal/sites/all/modules. Then enable the 
module in [Admin > Site building > Modules]. You will need to configure the 
user permissions before non-admin users can use this.


JAVASCRIPT
----------

This Drupal module acts as a wrapper for the Droplist Filter Javascript class,
which is included with this package in the "droplistFilter" subdirectory. It
is possible to use that independently from Drupal. See the documentation in the
subdirectory for more information.


USER INTERFACE
--------------

Four color schemes are provided. Their stylesheet and image files are found in
the following subdirectories:
 - UI_blue
 - UI_gray
 - UI_green
 - UI_red
Only one of them can be used any given time, and you may add your own files
to customize the UI for your site's theme. You must name your directory in the 
format of "UI_example". You can easily choose a different UI from the module's
configuration page. [Admin > Site configuration > Droplist Filter]



LEGAL
-----

Any graphics images included with this package are for example purposes only
and are copyright their respective owners.

    This class is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

